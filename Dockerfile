FROM node

WORKDIR /usr/share/ev-stock-trends

COPY . .

RUN npm install

CMD ["tail", "-f", "/dev/null"]