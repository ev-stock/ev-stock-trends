
const googleTrends = require('google-trends-api');
const EvOrmService = require('./EvOrmService');
const SocksProxyAgent = require('socks-proxy-agent');
const logging = require('../utils/logging');
const proxyAgent =  new SocksProxyAgent('socks5://10.102.160.00:9050');

let maxRetries = 0;
let retryCount = 0;

async function get(companies, from, to, interval, retries, proxy, fromDate, toDate) {
  await delay(300000); //waiting for nordVPN (if set in cronjob)
  maxRetries = retries;
  for (i=from; i<to; i++) {
    if (i >= companies.length) { break; }
    retryCount = 0;
    const company = companies[i];
    const options = { keyword: company.name, granularTimeResolution: true };
    if (proxy) { options.agent = proxyAgent; }
    if (fromDate) { options.startTime = fromDate; }
    if (toDate) { options.endTime = toDate; }

    let data = await getTrendForTerm(options);
    data = transformTrendData(data, company.id);
    if(data) { await postTrends(data, company); }

    logging.log(`Processed ${i+1}/${companies.length} companies`);
    await delay(interval);
  }
}

async function getTrendForTerm(options) {
  return await googleTrends.interestOverTime(options)
  .then(async results => {
      const data = JSON.parse(results);
      return data.default.timelineData;
  })
  .catch(async err => {
    if (err.requestBody && err.requestBody.includes("Error 429")) {
      retryCount++;
      if (retryCount > maxRetries) { 
        logging.log("Too many requests. Exiting...");
        process.exit();
      }
      logging.log(`Too many requests. Pausing for 20 seconds... ${retryCount}`);
      await delay(20000);
      await getTrendForTerm(options);
    } else {
      logging.log(err);
    }

    return null;
  });
}

function transformTrendData(data, companyId) {
  if (!data) { return null };

  let result = [];
  data.forEach(d => {
    result.push({ companyId: companyId, timestamp: d.time, value: d.value[0] });
  });
  return result;
}

async function postTrends(apiData, company) {
  if (apiData) {
    const dbData = await EvOrmService.sendGetToORM(`/trends?companyId=${company.id}`);
    const data = [];
    apiData.forEach(y => {
      const exists = dbData.filter(d => d.timestamp === parseInt(y.timestamp) && d.companyId === parseInt(y.companyId)).length > 0;
      if (!exists) { data.push(y); }
    });

    logging.log(`Found: ${data.length} new trends for company: ${company.code}`);
    await EvOrmService.sendDataChunksToORM('/trends/', 'post', data);
  }
}

function delay(time) {
  return new Promise(function(resolve) { 
    setTimeout(resolve, time)
  });
}

module.exports = {
  get,
}