const fetch = require('node-fetch');
const logging = require('../utils/logging');
const ormUrl = "http://192.168.0.44:30000";

async function sendDataChunksToORM(endpoint, method, data) {
  let chunk = 500;
  for (let i=0; i<data.length; i+=chunk) {
    tempdata = data.slice(i,i+chunk);
    await fetch(ormUrl + endpoint, {
      method:  method,
      body:    JSON.stringify(tempdata),
      headers: { 'Content-Type': 'application/json' },
    })
    .then((res) => logging.log(`Request to ${endpoint} successfully sent (${i+chunk > data.length ? data.length : i+chunk}/${data.length})!`))
    .catch((err) => logging.log(`Request to ${endpoint} failed.. ${err}`));
  }
}

function sendDataToORM(endpoint, method, data) {
  return fetch(ormUrl + endpoint, {
          method:  method,
          body:    JSON.stringify(data),
          headers: { 'Content-Type': 'application/json' },
        })
        .then((res) => logging.log(`Request to ${endpoint} successfully sent!`))
        .catch((err) => logging.log(`Request to ${endpoint} failed..`, err));
}

function sendGetToORM(endpoint) {
  const fetch = require('node-fetch');
  return fetch(ormUrl + endpoint)
    .then((res) => res.json())
    .catch((err) => logging.log(`Request to ${endpoint} failed.. ${err}`));
}

module.exports = {
  sendDataChunksToORM,
  sendDataToORM,
  sendGetToORM,
}