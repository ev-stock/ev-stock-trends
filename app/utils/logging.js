
function log(content) {
    const date = new Date().toISOString();
    console.log(`${date}: ${content}`);
}

module.exports = {
    log,
}