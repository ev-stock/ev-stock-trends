const EvOrmService = require('./services/EvOrmService');
const TrendsService = require('./services/TrendsService');
const logging = require('./utils/logging');

async function getCompanyTrends() {
  const args = process.argv.slice(2); //example: npm run trends 0 5 YAHOO_UK 1000 3 0 03/01/2019
  const from = parseInt(args[0]);
  const to = parseInt(args[1]);
  const source = args[2];
  const interval = parseInt(args[3]); //delay as google api gives 429 error on too many requests
  const retries = parseInt(args[4]);
  const proxy = parseInt(args[5]); //1: on, 0: off
  const fromDate = args[6] ? new Date(args[6]) : null;
  const toDate = args[7] ? new Date(args[7]) : null;

  logging.log(`Starting ev-stock-trends`);
  const companies = await EvOrmService.sendGetToORM(`/companies?source=${source}&orderby=code ASC`);
  //companies = [
  //  { id: 1, source: "YAHOO_USA", code: "DSN", name: "Disney"},
  //  { id: 2, source: "YAHOO_USA", code: "CO", name: "Coca Cola Company"},
  //  { id: 3, source: "YAHOO_UK", code: "HSBC.L", name: "HSBC PLC"},
  //  { id: 4, source: "YAHOO_UK", code: "AVA.L", name: "Aviva plc"},
  //];

  logging.log(`Got ${source} companies from orm`);
  await TrendsService.get(companies, from, to, interval, retries, proxy, fromDate, toDate);
}

getCompanyTrends();